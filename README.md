# Onboarding

# Inspirations
* [More than just first user experience](https://www.trychameleon.com/blog/user-onboarding-more-than-first-user-experience)
* [Onboarding UI Patterns](http://ui-patterns.com/patterns/onboarding/list)
* [C.A.R.E - a user onboarding framework by intercom.io](https://www.intercom.com/blog/c-a-r-e-simple-framework-user-onboarding/)
